package com.example.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.Nullable

class HelloService : Service() {

    override fun onCreate() {
        // 僅初次建立時呼叫
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        // 每次startService時會呼叫
        Log.d("HelloService", "onStartCommand Start")
        val endTime = System.currentTimeMillis() + 15 * 1000
        while (System.currentTimeMillis() < endTime) {
            synchronized(this) {
                try {
                    Thread.sleep(endTime - System.currentTimeMillis())
                } catch (e: Exception) {
                }
            }
        }
        Log.d("HelloService", "onStartCommand End")
        Toast.makeText(applicationContext, "End", Toast.LENGTH_SHORT).show()
//      停止Service  與IntentService(自動停止)不同  Service需要手動停止
        stopSelf()

        return START_STICKY
    }

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        Log.d("HelloService", "onDestroy")
    }

}